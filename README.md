### Techstack

- React.js
- Express.js
- MongoDB
- Graphql

# Important Notes

    1. English and German Language is implemented
    2. Reusable DataTable Module is implemented for GRAPHQL.
        # Only Pass Query Model and Query Field the Component Will handle the rest.
             <DataTable
                columns={this.columns}
                queryModel={"Customers"}
                queryFiled={"id name email"}
             />
        # You can Use an Export Function Called dataTable.reload() to reload the table.
    3. Some Reusable Function like checkAuth,getUser,checkRole is implemented for ease of use including Reusable function like "fetechGraphql" which automatically handle error and return Promise if result is ok.
    4. The frontend part is using ANTD and every functions, modules, components and layouts is implemented from scratch.



# Implemented Module

    1. Add/remove/update/view customer
    2. Activity Log Viewer Role Wise
    6. Role base Access reusable access module for both frontend and backend.
    7. Mongo DB Seeder for testing
    8. Login and Register module for user

### Every query in this project is using GraphQL including user authentication and role based access. The project using a boilerplate also created by me which is using stack of Express-GraphQL-Mongo (https://github.com/sangit0/Express-graphql-mongodb-boilerplate).

### How to run

    cd /to project folder
    yarn
    yarn start
    #open new terminal
    cd backend
    yarn
    yarn start

For mongo you can use your own cluster. But if you want to run easily and check the project, i have provided my mongodb database URI from mongo cloud.

## How to use the Seeder Commands

### Before running the seed or running the backend copy .env.example to .env and setup your Database information.

            cd to backend folder
            yarn md-seed run
            for dropping the database
            yarn md-seed run --dropdb

### Login (if you use DB seed)

    user: admin@sangit.info  (admin user)
    pass: secret

    user: user2@gmail.info  (normal user)
    pass: secret
