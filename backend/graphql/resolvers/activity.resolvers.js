var model = require("../../models/activity.model");

module.exports = {
  Query: {
    // Activities: () => model.find().exec(),
    Activity: (obj, args) => {
      return model.find(args).exec();
    },
    NotificationCount: (obj, args) => {
      if (args.admin) {
        return model.find({ read_status: false }).count();
      }
      return model.find({ user_id: args.user_id, read_status: false }).count();
    },
  },
  Mutation: {
    //for self register
    markAsReadAll: async (source, args) => {
      // model.updateMany({ user_id: args.id }, { $set: { read_status: true } });
      model
        .update(
          { user_id: args.id },
          { $set: { read_status: true } },
          { upsert: true, multi: true }
        )
        .exec();
    },
  },
};
