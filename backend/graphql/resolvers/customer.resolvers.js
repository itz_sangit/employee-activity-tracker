var User = require("../../models/user.model");
var Activity = require("../../models/activity.model");

const bcrypt = require("bcryptjs");

module.exports = {
  Query: {
    //only users with role of user
    Customers: () =>
      User.find({
        role: "user",
      }).exec(),
    Customer: (obj, args) => {
      return User.find({ id: args.id }).exec();
    },
  },
  Mutation: {
    //for self register
    createCustomer: async (source, args) => {
      try {
        args = args.customerInput;
        const user = await new User(args).save();
        activity = Activity({
          action: "created",
          user_id: user.id,
        }).save();
        return user;
      } catch (error) {
        return User.checkDuplicateEmailError(error);
      }
    },
    updateCustomer: async (source, params) => {
      try {
        // conso  le.log(params);

        user = User.findByIdAndUpdate(
          params.id,
          {
            $set: {
              name: params.customerInput.name,
              email: params.customerInput.email,
              phone_type: params.customerInput.phone_type,
              phone: params.customerInput.phone,
              email_type: params.customerInput.email_type,
            },
          },
          {
            new: true,
          }
        );
        activity = new Activity({
          action: "updated",
          user_id: params.id,
        }).save();
        return user;
      } catch (error) {
        return User.checkDuplicateEmailError(error);
      }
    },

    deleteCustomer: async (source, params) => {
      try {
        // conso  le.log(params);

        return User.findByIdAndDelete(params.id);
      } catch (error) {
        return User.checkDuplicateEmailError(error);
      }
    },
  },
};
