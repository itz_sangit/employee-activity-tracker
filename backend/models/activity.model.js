var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ActivityLogs = new Schema(
  {
    action: {
      type: String,
      enum: ["created", "updated"],
      default: "created",
    },
    read_status: {
      type: Boolean,
      enum: [true, false],
      default: false,
    },
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);
var Model = mongoose.model("ActivityLogs", ActivityLogs);
module.exports = Model;
