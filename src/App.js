import React, { Component, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import { Login } from "./components/login";
import { Register } from "./components/register";
import { HomePage } from "./components/home";
import { Customer } from "./components/customer/index.customer";
import { ActivityLogs } from "./components/activity/index.activity";
import { PrivateRoute } from "./_helpers/PrivateRoute";
import { PublicRoute } from "./_helpers/PublicRoute";
import { checkAuth } from "./_helpers/auth";
import { withRouter } from "react-router-dom";
import { UserNavBar } from "./components/layout/userNavBar";
import { NoPermission } from "./components/layout/403";
import { createStore } from "@spyna/react-store";
import "./i18n";

import { roles } from "./_helpers/role";

import {
  HeaderContent,
  FooterContent,
} from "./components/layout/header-footer";

import { Layout } from "antd";

const initialValue = {
  notification_counter: 0,
};

const { Content } = Layout;
const NoMatch = ({ location }) => (
  <div>
    <h3>
      No match for <code>{location.pathname}</code>
    </h3>
  </div>
);
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: checkAuth(),
    };
  }

  componentDidMount() {}
  componentWillMount() {
    if (this.state.isAuthenticated !== checkAuth()) {
      this.setState({ isAuthenticated: checkAuth() });
    }
  }
  render() {
    return (
      <div>
        <Suspense fallback={null}>
          <Layout>
            <UserNavBar
              location={this.props.location}
              history={this.props.history}
              isAuthenticated={this.state.isAuthenticated}
            />

            <Layout className="site-layout">
              <HeaderContent />

              <Content style={{ margin: "24px 16px 0" }}>
                <Switch>
                  <PrivateRoute
                    exact
                    path="/"
                    component={withRouter(HomePage)}
                  />
                  <PrivateRoute
                    exact
                    path="/customer"
                    role={roles.ADMIN}
                    component={withRouter(Customer)}
                  />
                  <PrivateRoute
                    exact
                    path="/activity-log"
                    role={roles.ALL_USER}
                    component={withRouter(ActivityLogs)}
                  />
                  <PublicRoute exact path="/login" component={Login} />
                  <Route exact path="/403" component={NoPermission} />
                  <PublicRoute exact path="/register" component={Register} />
                  <Route component={NoMatch} />
                </Switch>
                <FooterContent />
              </Content>
            </Layout>
          </Layout>
        </Suspense>
      </div>
    );
  }
}
export default createStore(withRouter(App), initialValue);
