import fetchGraphQL from "../_helpers/fetchGraphQL";

export const activityService = {
  get,
  notificationCount,
  markAsReadAll,
};

function get(id) {
  return fetchGraphQL(`
        {
            Activity(user_id:"${id}"){
                 action createdAt
            }
        }
    `);
}

function notificationCount(id, admin) {
  return fetchGraphQL(`
        {
           NotificationCount(user_id:"${id}",admin:${admin})
        }
    `);
}
function markAsReadAll(id) {
  return fetchGraphQL(`
                mutation {
                  markAsReadAll(id:"${id}")
            }
    `);
}
