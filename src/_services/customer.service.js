import fetchGraphQL from "../_helpers/fetchGraphQL";

export const customerService = {
  create,
  getAll,
  update,
  remove,
};

function create({ email, password, name, phone_type, phone, email_type }) {
  return fetchGraphQL(`
                mutation {
                  createCustomer(customerInput:{email:"${email}",password:"${password}",name:"${name}", phone:"${phone}",phone_type:"${phone_type}",email_type:"${email_type}"}){
                    id name role
                }
            }
    `);
}
function update({ id, email, name, phone_type, phone, email_type }) {
  return fetchGraphQL(`
                mutation {
                  updateCustomer(id:"${id}",customerInput:{email:"${email}",name:"${name}", phone:"${phone}", phone_type:"${phone_type}",email_type:"${email_type}"}){
                    id name role
                }
            }
    `);
}

function remove({ id }) {
  return fetchGraphQL(`
                mutation {
                  deleteCustomer(id:"${id}"){
                    id name role
                }
            }
    `);
}

function getAll() {
  return fetchGraphQL(`
                 {
                  Customers{
                    id name email role 
                  }
                }
    `);
}
