import React, { useState, useEffect } from "react";
import { Card, Collapse, Spin } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";
import { customerService } from "../../_services/customer.service";
import { activityService } from "../../_services/activity.service";
import { checkRole } from "../../_helpers/auth";
import { LogsMapper } from "./logs";
import moment from "moment";
import { getUser } from "../../_helpers/auth";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

const columns = [
  {
    title: "",
    dataIndex: "log",
    key: "log",
  },
];
export const ActivityLogs = (props) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState(false);
  const [loadingActivity, setLoadingActivity] = useState(false);

  const [data, setData] = useState([]);
  const [customer, setCustomer] = useState([]);

  const loadData = () => {
    setLoading(true);

    customerService.getAll().then((response) => {
      setCustomer(response.data["Customers"]);
      setLoading(false);
    });
  };
  useEffect(() => {
    if (checkRole(["admin"])) {
      loadData();
    } else {
      loadActivity(getUser().id);
    }
  }, []);

  const loadActivity = (e) => {
    if (!e) {
      return;
    }
    setLoadingActivity(true);

    activityService.get(e).then((response) => {
      let mapped = [];
      let log = response.data["Activity"];
      mapped = log.map((e) => {
        return {
          log:
            t("menu.customer") +
            " " +
            t(e.action) +
            " at " +
            moment(parseInt(e.createdAt)).format("lll"),
          action: e.action,
        };
      });
      setLoadingActivity(false);

      setData(mapped);
    });
  };

  return (
    <Card style={{ width: "100%" }} className="site-layout-background ">
      <h3>{t("menu.activity")}</h3>
      <hr></hr>
      {checkRole(["admin"]) ? (
        customer.length ? (
          <Collapse
            bordered={false}
            accordion
            onChange={(e) => loadActivity(e)}
            expandIcon={({ isActive }) => (
              <CaretRightOutlined rotate={isActive ? 90 : 0} />
            )}
            className="site-collapse-custom-collapse"
          >
            {customer.map((e, index) => (
              <Panel
                header={<h5>{e.name}</h5>}
                key={e.id}
                className="site-collapse-custom-panel"
              >
                <LogsMapper data={data} loading={loadingActivity} />
              </Panel>
            ))}
          </Collapse>
        ) : loading ? (
          <Spin />
        ) : (
          <h3>{t("no_data.message")}</h3>
        )
      ) : (
        <LogsMapper data={data} loading={loadingActivity} />
      )}
    </Card>
  );
};
