import React from "react";
import { Card, Tag, Spin } from "antd";
import { useTranslation } from "react-i18next";

export const LogsMapper = (props) => {
  const { t } = useTranslation();

  return (
    <Card style={{ width: "100%" }} className="site-layout-background ">
      {!props.loading ? (
        props.data.length ? (
          props.data.map((e, index) => (
            <span key={index + "span"}>
              <Tag color={e.action === "created" ? "green" : "red"} key={index}>
                {e.log}
              </Tag>
              <br></br>
            </span>
          ))
        ) : (
          <h4>{t("no_data.message")}</h4>
        )
      ) : (
        <Spin />
      )}
    </Card>
  );
};
