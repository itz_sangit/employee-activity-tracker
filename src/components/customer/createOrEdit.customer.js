import React from "react";
import { Modal, Button, Form, Input, Alert, Select } from "antd";
import { useTranslation } from "react-i18next";
const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
export const CreateOrEditCustomer = (props) => {
  const { t } = useTranslation();
  const afterSelector = (type) => (
    <Form.Item name={type} noStyle>
      <Select style={{ width: 100 }}>
        <Option value="Personal">{t("personal")}</Option>
        <Option value="Official">{t("official")}</Option>
      </Select>
    </Form.Item>
  );
  return (
    <Modal
      title={props.title}
      width="500px"
      visible={props.visible}
      confirmLoading={props.confirmLoading}
      onCancel={props.handleCancel}
      footer={[
        <Button key="back" onClick={props.handleCancel}>
          Return
        </Button>,
      ]}
    >
      <Alert
        message={t("information")}
        description={t("customer.info")}
        type="success"
      />
      <br></br>
      <Form
        {...layout}
        name="Customer"
        onFinish={props.handleOk}
        initialValues={{
          email_type: "Official",
          phone_type: "Official",
        }}
        ref={props.formRef}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: t("input.message") }]}
        >
          <Input addonAfter={afterSelector("email_type")} />
        </Form.Item>
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: t("input.message") }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={t("phone")}
          name="phone"
          rules={[{ required: true, message: t("input.message") }]}
        >
          <Input addonAfter={afterSelector("phone_type")} />
        </Form.Item>

        {props.data.id === "new" ? (
          <Form.Item
            label={t("password") + "(123456)"}
            name="password"
            rules={[{ required: true, message: t("input.message") }]}
          >
            <Input.Password />
          </Form.Item>
        ) : (
          ""
        )}

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            {props.data.id === "new" ? t("create.label") : t("update.label")}{" "}
            {t("menu.customer")}
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};
