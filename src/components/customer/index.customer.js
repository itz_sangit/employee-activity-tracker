import React from "react";
import { Card, Button, Space, notification } from "antd";
import { CreateOrEditCustomer } from "./createOrEdit.customer";
import { customerService } from "../../_services/customer.service";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { withStore } from "@spyna/react-store";
import { withTranslation } from "react-i18next";
import { DataTable, dataTable } from "../datatable/index.table";

class CustomerComp extends React.Component {
  formRef = React.createRef();
  _mounted = false;
  fields = {
    id: "new",
    name: "",
    email: "",
  };
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      loading: false,
      visible: false,
      dataModel: {
        ...this.fields,
      },
      confirmLoading: false,
    };
  }

  columns = [
    {
      title: this.props.t("name"),
      dataIndex: "name",
      key: "name",
    },
    {
      title: this.props.t("email"),
      key: "email",
      render: (text, record) => (
        <span>
          {record.email ? record.email + "(" + record.email_type + ")" : "N/A"}
        </span>
      ),
    },
    {
      title: this.props.t("phone"),
      key: "phone",
      render: (text, record) => (
        <span>
          {record.phone ? record.phone + "(" + record.phone_type + ")" : "N/A"}
        </span>
      ),
    },
    {
      title: this.props.t("action"),
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            shape="circle"
            icon={<EditOutlined />}
            onClick={() => this.editMode(record)}
          />
          <Button
            shape="circle"
            icon={<DeleteOutlined />}
            onClick={() => this.deleteRecord(record)}
          />
        </Space>
      ),
    },
  ];
  componentDidMount() {
    this._mounted = true;
  }
  componentWillUnmount() {
    this._mounted = false;
  }

  handleOk = (values) => {
    this._mounted &&
      this.setState({
        confirmLoading: true,
      });
    if (this.state.dataModel.id === "new") {
      customerService.create(values).then((response) => {
        this._mounted &&
          this.setState({
            confirmLoading: true,
            visible: false,
          });
        this._mounted && this.incrementNotification();

        dataTable.reload();
        notification.success({
          message: "Success",
          description: this.props.t("create.message"),
        });
      });
    } else {
      customerService
        .update({ id: this.state.dataModel.id, ...values })
        .then((response) => {
          this._mounted &&
            this.setState({
              confirmLoading: true,
              visible: false,
              dataModel: {
                ...this.fields,
              },
            });
          this._mounted && this.incrementNotification();

          dataTable.reload();
          notification.success({
            message: "Success",
            description: this.props.t("update.message"),
          });
        });
    }
  };
  incrementNotification = () => {
    let counter = this.props.notification_counter;
    this.props.store.set("notification_counter", ++counter);
  };
  handleCancel = () => {
    this._mounted &&
      this.setState({
        visible: false,
      });
  };

  showModal = () => {
    this._mounted &&
      this.setState({
        dataModel: {
          ...this.fields,
        },
        visible: true,
      });
  };

  editMode = (record) => {
    // this.showModal();
    this._mounted &&
      this.setState(
        {
          dataModel: record,
          visible: true,
        },
        () => {
          if (this.formRef.current) {
            this.formRef.current.setFieldsValue({
              ...record,
            });
          } else {
            setTimeout(() => {
              this.formRef.current &&
                this.formRef.current.setFieldsValue({
                  ...record,
                });
            }, 500);
          }
        }
      );
  };
  deleteRecord = (record) => {
    customerService.remove(record).then((response) => {
      dataTable.reload();
    });
  };

  render() {
    return (
      <Card style={{ width: "100%" }} className="site-layout-background ">
        <Button type="primary" onClick={this.showModal}>
          {this.props.t("create.customer.btn")}
        </Button>
        <hr></hr>

        <CreateOrEditCustomer
          handleCancel={this.handleCancel}
          handleOk={this.handleOk}
          visible={this.state.visible}
          formRef={this.formRef}
          data={this.state.dataModel}
          confirmLoading={this.state.confirmLoading}
        />
        <DataTable
          columns={this.columns}
          queryModel={"Customers"}
          queryFiled={"id name email phone role phone_type email_type"}
        />
      </Card>
    );
  }
}

const Customer = withTranslation()(
  withStore(CustomerComp, ["notification_counter"])
);

export { Customer };
