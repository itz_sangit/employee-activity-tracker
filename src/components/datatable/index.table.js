import React, { useState, useEffect } from "react";
import { Table } from "antd";
import fetchGraphQL from "../../_helpers/fetchGraphQL";

export const dataTable = {
  reload: null,
};
export const DataTable = (props) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    loadData();
  }, []);
  const loadData = () => {
    setLoading(true);
    fetchGraphQL(`
        {${props.queryModel}{
            ${props.queryFiled}
            }
         }
    `).then((response) => {
      setData(response.data[props.queryModel]);
      setLoading(false);
    });
  };
  dataTable.reload = loadData;

  return (
    <Table
      rowKey="id"
      columns={props.columns}
      dataSource={data}
      loading={loading}
    />
  );
};
