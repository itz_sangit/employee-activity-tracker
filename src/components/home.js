import React from "react";
import { Card } from "antd";
import { withTranslation } from "react-i18next";
import { checkRole } from "../_helpers/auth";
import { Customer } from "./customer/index.customer";

class HomePageComp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
    };
  }

  render() {
    return (
      <div className="site-layout-background ">
        <Card>
          <h1>{this.props.t("welcome.message")}</h1>

          {checkRole(["admin"]) && <Customer />}
        </Card>
      </div>
    );
  }
}

const HomePage = withTranslation()(HomePageComp);
export { HomePage };
