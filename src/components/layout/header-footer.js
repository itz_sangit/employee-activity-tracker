import React, { useState, useEffect } from "react";
import { Layout, Badge, message, Radio } from "antd";
import { NotificationOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import { withStore } from "@spyna/react-store";
import { withRouter } from "react-router-dom";
import { checkRole } from "../../_helpers/auth";

import { getUser } from "../../_helpers/auth";
import { activityService } from "../../_services/activity.service";

const { Header, Footer } = Layout;

const logs = (props) => {
  props.store.get("notification_counter") !== 0 &&
    message.info("Notification Cleared");

  props.store.set("notification_counter", 0);
  props.history.push("/activity-log");

  if (getUser()) {
    activityService.markAsReadAll(getUser().id).then((response) => {});
  }
};
const HeaderContentComponent = (props) => {
  const { t, i18n } = useTranslation();
  const [lang, setLang] = useState("en");

  const changeLanguage = (event) => {
    i18n.changeLanguage(event.target.value);
    setLang(event.target.value);
  };
  useEffect((e) => {
    if (getUser().id !== undefined) {
      activityService
        .notificationCount(getUser().id, checkRole(["admin"]))
        .then((response) => {
          props.store.set(
            "notification_counter",
            response.data["NotificationCount"]
          );
        });
    }
  }, []);
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      <span style={{ float: "right", marginTop: "10px", marginRight: "20px" }}>
        <Radio.Group value={lang} onChange={changeLanguage} size="small">
          <Radio.Button value="en">EN</Radio.Button>
          <Radio.Button value="de">DE</Radio.Button>
        </Radio.Group>
      </span>
      <span style={{ float: "right", marginTop: "10px", marginRight: "20px" }}>
        <Badge count={props.notification_counter} title="User activity logs">
          <span onClick={() => logs(props)} style={{ cursor: "pointer" }}>
            <NotificationOutlined />
          </span>
        </Badge>
      </span>
    </Header>
  );
};
export const HeaderContent = withStore(withRouter(HeaderContentComponent), [
  "notification_counter",
]);
export const FooterContent = (props) => {
  return (
    <Footer style={{ textAlign: "center", position: "sticky" }}>
      Sangit © 2020 Selise Employee Tracker
    </Footer>
  );
};
