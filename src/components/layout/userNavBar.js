import React from "react";
import { userService } from "../../_services/user.service";
import { Layout, Menu, Avatar, Popover, Divider } from "antd";
import {
  UserOutlined,
  HomeOutlined,
  ExperimentOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { roles } from "../../_helpers/role";
import { checkRole } from "../../_helpers/auth";
import { Tag } from "antd";
import { getUser } from "../../_helpers/auth";
import { useTranslation } from "react-i18next";

const { Sider } = Layout;

const routeLink = [
  {
    url: "/",
    name: "menu.home",
    icon: <HomeOutlined />,
  },
  {
    url: "/customer",
    name: "menu.customer",
    role: roles.ADMIN,
    icon: <UserOutlined />,
  },
  {
    url: "/activity-log",
    name: "menu.activity",
    role: roles.ALL_USER,
    icon: <ExperimentOutlined />,
  },
];
const userMenuOptions = (props) => (
  <Menu>
    <Menu.Item onClick={() => userService.logout()}>Logout</Menu.Item>
  </Menu>
);
export const UserNavBar = (props) => {
  const { t } = useTranslation();

  if (props.isAuthenticated) {
    return (
      <Sider
        className="main-container"
        breakpoint="lg"
        theme="dark"
        collapsedWidth="0"
      >
        <span className="logo">{t("site.name")}</span>
        <hr></hr>

        <div style={{ textAlign: "center", marginTop: "10%" }}>
          <Popover
            placement="bottom"
            style={{ cursor: "pointer" }}
            content={userMenuOptions(props)}
            trigger="hover"
          >
            <span>
              <Avatar
                icon={
                  <UserOutlined
                    style={{ textAlign: "center", cursor: "pointer" }}
                  />
                }
              />
            </span>
          </Popover>
          <br></br>
          <br></br>
          <Tag>
            {" "}
            {t("hello.label")} {getUser().name}
          </Tag>
          <Divider></Divider>
        </div>

        <Menu theme="dark" mode="inline" defaultSelectedKeys={["4"]}>
          {routeLink.map((element) => {
            return checkRole(element.role) ? (
              <Menu.Item key={element.name} icon={element.icon}>
                <Link to={element.url}>{t(element.name)}</Link>
              </Menu.Item>
            ) : (
              ""
            );
          })}
        </Menu>
      </Sider>
    );
  } else {
    return <GuestNavBar location={props.location} />;
  }
};

export const GuestNavBar = (props) => {
  return <span></span>;
};
