import React from "react";
import { Form, Input, Button, Card, Row, Col } from "antd";
import { userService } from "../_services/user.service";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

export const Login = () => {
  const { t } = useTranslation();

  const onFinish = (values) => {
    userService.login({ ...values }).then(
      (user) => {
        const { from } = this.props.location.state || {
          from: { pathname: "/" },
        };
        this.props.history.push(from);
      },
      (error) => console.log(error)
    );
  };

  return (
    <Row
      type="flex"
      justify="center"
      align="middle"
      style={{ minHeight: "80vh" }}
    >
      <Col>
        <Card title={t("login.label")}>
          <h3>{t("site.name")}</h3>
          <hr></hr>
          <Form {...layout} name="Login" onFinish={onFinish}>
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  type: "email",
                  message: t("input.message"),
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label={t("password")}
              name="password"
              rules={[
                {
                  required: true,
                  message: t("input.message"),
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                {t("login.label")}
              </Button>
            </Form.Item>
          </Form>
          <Link to="/register">{t("login.info")}</Link>
        </Card>
      </Col>
    </Row>
  );
};
